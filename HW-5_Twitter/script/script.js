const usersUrl = "https://ajax.test-danit.com/api/json/users";
const postsUrl = "https://ajax.test-danit.com/api/json/posts";
const container = document.querySelector('.container');
const cardPost = document.querySelector(`card_post-${this.id}`);

class UsersCard {
    constructor(name, email, title, body, id, userId) {
        this.name = name;
        this.email = email;
        this.title = title;
        this.body = body;
        this.id = id;
        this.userId = userId;
        this.card = document.createElement('div');
        this.card.className = `card card_post-${this.id}`;
        this.buttonDelete = document.createElement('button');
        this.buttonDelete.className = "del-btn";

    }

    render() {
        container.append(this.card);
        this.card.insertAdjacentHTML("beforeend",
            `<span class="name">${this.name}"</span>
            <a href="mailto:${this.email}" class="email">Email: ${this.email}</a>
            <h4 class="title"> Post: ${this.title}</h4>
            <p class="text">Text: ${this.body}</p>`);

        this.buttonDelete.innerHTML = 'Delete'
        this.card.append(this.buttonDelete);
        this.buttonDelete.addEventListener('click', () => {
            fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`)
                .then((res) => {
                    if (res.status === 200) {
                        this.card.remove();
                    }
                })
        })
    }
}

// --------------------------

const getPostsCard = async() => {
    try {
        const responsePosts = await fetch(postsUrl).then(res => res.json())
        return responsePosts;
    } catch (error) {
        console.warn(error);
    }
}

const getUsersCard = async() => {
    try {
        const responseUsers = await fetch(usersUrl).then(res => res.json())
        return responseUsers;
    } catch (error) {
        console.warn(error);
    }
}

// ----------------------------

const showCards = async() => {
    try {
        const postsArray = await getPostsCard();
        const usersArray = await getUsersCard();

        postsArray.forEach(({ title, body, id, userId }) => {
            usersArray.forEach(({ name, email, id: numUser }) => {
                if (numUser === userId) {
                    new UsersCard(name, email, title, body, id, userId).render();
                }
            })
        });
    } catch (error) {
        console.warn(error);
    }
}

showCards();