const btnGetIp = document.querySelector('.btn-get-ip');
const container = document.querySelector('.container');

async function getIp() {
    const result = await fetch('https://api.ipify.org/?format=json').then(res => res.json());
    return result;
}
const getIpUser = getIp();

function getDataUser(getIpUser) {

    getIpUser.then(async function(result) {
        const data = await fetch(`http://ip-api.com/json/${result.ip}?fields=continent,country,region,city,district`).then(res => res.json());
        container.innerHTML =
            `<ul>
            <li>Континент: ${data.continent}</li>
             <li>Країна: ${data.country}</li>
             <li>Pегіон: ${data.region}</li>
             <li>Mісто: ${data.city}</li>
             <li>Pайон: ${data.district}</li>
             </ul>`
    });
}

btnGetIp.addEventListener('click', (e) => {
    getDataUser(getIpUser);
})