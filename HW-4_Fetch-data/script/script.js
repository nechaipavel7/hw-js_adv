const url = 'https://ajax.test-danit.com/api/swapi/films';

fetch(url)
    .then(response => response.json())
    .then(data => {
        data.forEach(({ episodeId, name, openingCrawl, characters }) => {
            document.querySelector(".container").insertAdjacentHTML("beforeend",
                `<div class="card">
            <h1 class="title"> Episode: ${episodeId} <br> " ${name}"</h1>
            <p class="title-text">${openingCrawl}</p>
            <p class="list">List of characters:</p>
            <ul class=" list list_episode-${episodeId}">
            </ul>
            </div>`)

            characters.forEach((charactersUrl) => {
                fetch(charactersUrl)
                    .then(response => response.json())
                    .then(value => {
                        document.querySelector(`.list_episode-${episodeId}`).insertAdjacentHTML("beforeend", `<li class="list-characters">-${value.name}</li>`)
                    })
                    .catch(error => console.warn(error))
            })
        });
    })
    .catch(error => console.warn(error))



// ---------------------------------------------------------------------------------
// <span class="loader"></span>  (preloader-працює тільки з innerText, але криво )
// .insertAdjacentHTML("beforeend", `<li class="list-characters">-${value.name}</li>`)
// .innerText += value.name
// .innerHTML += (`<li class="list-characters">-${value.name}</li>`)