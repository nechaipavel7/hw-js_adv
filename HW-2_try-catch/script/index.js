const books = [{
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",

    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const rootBooksList = document.querySelector('#root');

class BooksListError extends Error {
    constructor(value) {
        super()
        this.name = "BooksListError";
        this.message = `Invalid format BooksList, no value entered: ${value}`;
    }
}

class BooksList {
    constructor({ author, name, price }) {
        if (author === undefined) {
            throw new BooksListError("author");
        }
        if (name === undefined) {
            throw new BooksListError("name");
        }
        if (price === undefined) {
            throw new BooksListError("price");
        }
        this.author = author;
        this.name = name;
        this.prise = price;

    }



    render(rootBooksList) {
        rootBooksList.insertAdjacentHTML('beforeend',
            `<ul>
                <li>Autor : ${this.author} </li>
                <li>Name : ${this.name} </li>
                <li>Prise : ${this.prise} </li>
        </ul>`)
    }
}

books.forEach((element) => {
    try {
        new BooksList(element).render(rootBooksList);
    } catch (err) {
        if (err.name === "BooksListError") {
            console.warn(err);
        } else {
            throw err;
        }
    }

});