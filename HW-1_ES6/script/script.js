class Employee {
    constructor(name, age, salaru) {
        this._name = name;
        this._age = age;
        this._salaru = salaru;
    }

    get name() {
        return this._name.toUpperCase();
    }

    set name(newName) {
        this._name = newName;
    }

    get age() {
        return this._age;
    }

    set age(newAge) {
        this._age = newAge;
    }

    get salaru() {
        return this._salaru;
    }

    set salaru(newSalaru) {
        this._salaru = newSalaru;
    }
}

class Programmer extends Employee {
    constructor(lang, name, age, salaru, ) {
        super(name, age, salaru)
        this.lang = lang
    }

    get salaru() {
        return super.salaru * 3;
        // return this._salaru * 3;
    }

    // для можливості перезадати значення salaru в 64 строчці
    set salaru(newSalaru) {
        super.salaru = newSalaru;

    }
}


const ed = new Employee('ed', 34, 1000);


console.log(ed);
console.log(ed.name);
console.log(ed.salaru);


const pit = new Programmer('js', 'pit', 31, 3000);
console.log(pit);
console.log(pit.salaru);

pit.salaru = 600;
console.log(pit.salaru);


const bob = new Programmer('java', 'bob', 27, 2500);
console.log(bob);
console.log(bob.name);
console.log(bob.lang);
console.log(bob.salaru);

const jeck = new Programmer("c++", 'jeck', 45, 4000);
console.log(jeck);
console.log(jeck.salaru);